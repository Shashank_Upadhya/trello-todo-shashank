const auth = '?key=47fb1216aa2b59b43511a4ccaf057b11&token=f50e2ab0efbd48426009c77d6a91364025a192a544151e1b6018331ca18a7275';
const auth1 = '&key=47fb1216aa2b59b43511a4ccaf057b11&token=f50e2ab0efbd48426009c77d6a91364025a192a544151e1b6018331ca18a7275';
const site="https://api.trello.com/1/";
let url = site+"members/me/boards" + auth;

function getDate() {
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const d = new Date();
  let date = `${months[d.getMonth()]} ${d.getDate()}`;
  return date;
}

function loadData() {
  $.getJSON(url).then((boards) => {
    boards.forEach((board) => {
      const cardUrl = `${site}boards/${board.id}/cards${auth}`;
      $.getJSON(cardUrl).then((cards) => {
        cards.forEach((card) => {
          const getCard = `${site}cards/${card.id}${auth}`;
          $.getJSON(getCard).then((cardDetails) => {
            let checklists = `${site}cards/${cardDetails.id}/checklists${auth}`;
            $.getJSON(checklists).then((checklistItems) => {
              let checklistKeys = Object.keys(checklistItems);
              checklistKeys.forEach((checklistItem) => {
                let checkItem = checklistItems[checklistItem].checkItems;
                let checklistItemKeys = Object.keys(checkItem);
                checklistItemKeys.forEach((checkItemKey) => {
                  if (checkItem[checkItemKey].state == 'incomplete') {
                    let value = `${card.id}|${checkItem[checkItemKey].idChecklist}|${checkItem[checkItemKey].id}`;
                    $('div').append(`<section><label class='label-container'><input type='checkbox' id='done' value="${value}" /><span class="task-names">${checkItem[checkItemKey].name} - ${card.name}</span><span class='checkmark'></span></label><button type="button" value="${value}" class="close" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button></section>`);
                  }
                });
              });
            });
          });
        });
      });
    });
  });
}
$(loadData);

function postData(postUrl, data) {
  return fetch(postUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    body: JSON.stringify(data),
  }).then(response => response.json());
}

function getData(postUrl) {
  return fetch(postUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
  }).then(response => response.json());
}

$('#addItems').live('keypress', function eventHandler(e) {
  if (e.keyCode == '13') {
    if (this.value != '') {
      let name = this.value;
      let date = getDate();
      $.getJSON(url).then((boards) => {
        let board = boards.filter(b => b.name == 'Shashank');
        let listurl = `${site}boards/${board[0].id}/lists${auth}`;
        $.getJSON(listurl).then((lists) => {
          let list = lists.filter(l => l.name == 'Todos');
          let cardurl = `${site}lists/${list[0].id}/cards?fields=id,name${auth1}`;
          $.getJSON(cardurl).then((cards) => {
            let cardObj = {};
            cards.forEach((card) => {
              cardObj[card.name] = card.id;
            });
            if (!(date in cardObj)) {
              postData(`${site}cards?name=${date}&pos=top&idList=${list[0].id}&keepFromSource=all${auth1}`, {})
                .then((card) => {
                  let cardId = card.id;
                  let cardName = card.name;
                  postData(`${site}cards/${cardId}/checklists${auth}`, {})
                    .then((checkList) => {
                      let checklistId = checkList.id;
                      postData(`${site}checklists/${checklistId}/checkItems?name=${name}&pos=top${auth1}`, {})
                        .then((checklistItem) => {
                          let value = `${cardId}|${checklistId}|${checklistItem.id}`;
                          $('div').append(`<section><label class='label-container'><input type='checkbox' id='done' value="${value}" /><span class="task-names">${checklistItem.name} - ${cardName}</span><span class='checkmark'></span></label><button type="button" value="${value}" class="close" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button></section>`);
                          document.getElementById('addItems').value = '';
                          document.getElementById('addItems').blur();
                        });
                    });
                });
            } else {
              let cardId = cardObj[date];
              getData(`${site}cards/${cardId}/checklists${auth}`)
                .then((checkList) => {
                  let checklistId = checkList[0].id;
                  postData(`${site}checklists/${checklistId}/checkItems?name=${name}&pos=top${auth1}`, {})
                    .then((checklistItem) => {
                      let value = `${cardId}|${checklistId}|${checklistItem.id}`;
                      $('div').append(`<section><label class='label-container'><input type='checkbox' id='done' value="${value}" /><span class="task-names">${checklistItem.name} - ${date}</span><span class='checkmark'></span></label><button type="button" value="${value}" class="close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></section>`);
                      document.getElementById('addItems').value = '';
                      document.getElementById('addItems').blur();
                    });
                });
            }
          });
        });
      });
    }
  }
});

$('.close').live('click', function clickListener() {
  if (confirm('Do you really want to delete the task?')) {
    let val = this.value;
    let ids = val.split('|');
    this.parentElement.style.display = 'none';
    fetch(`${site}cards/${ids[0]}/checkItem/${ids[2]}${auth}`, {
      method: 'DELETE',
    });
  }
});

$('#done').live('change', function checkListener() {
  let req = '';
  if ($(this).is(':checked')) {
    let val = this.value;
    let ids = val.split('|');
    req = `${site}cards/${ids[0]}/checkItem/${ids[2]}?state=complete${auth1}`;
  } else {
    let val = this.value;
    let ids = val.split('|');
    req = `${site}cards/${ids[0]}/checkItem/${ids[2]}?state=incomplete${auth1}`;
  }
  fetch(req, {
    method: 'PUT',
  });
});
